#include <iostream>
#include<math.h>

using namespace std;
// funcion que imprime una matriz n*n cuadrada que da el usuario y verifica a la vez, si es un cubo magico
int main()
{int matriz[100][100],columnas,filas,sumadecuadradomagico,numeroelevado,suma_de_columnas=0,suma_de_filas=0,suma_de_diagonales=0,suma_de_diagonales2=0;
    cout << "Ingrese el numero de filas y columnas de la matriz cuadrada: " << endl;
    cin>>filas;
    columnas=filas;//se iguala el numero de filas a columnas para que sea una matriz cuadrada
    for(int i=0;i<filas;i++){//ciclo que va poniendo los numeros ingresados por el usuario en las posiciones de la matriz
        for(int z=0;z<columnas;z++){
            cout<<"ingrese numeros, el numero que se ingresa se guarda en la posicion: ["<<i<<"]  ["<<z<<"]"<<endl;
            cin>>matriz[i][z];
        }
    }
    for(int i=0;i<filas;i++){//ciclo que imprime la matriz
        for(int z=0;z<columnas;z++){
            cout<<matriz[i][z];
        }
        cout<<endl;
    }
    numeroelevado=pow(filas,2);
    sumadecuadradomagico=(filas*(numeroelevado+1))/2;//operacion para calcular el numero que da si es un cubo magico
    for(int i=0;i<filas;i++){//ciclo para sumar las filas y luego columnas para luego verificar si es igual a lo que tien que dar para que sea un cuadrado magico
        suma_de_columnas=0;
        suma_de_diagonales=0;
        suma_de_filas=0;
        suma_de_diagonales2=0;
        for(int z=0;z<columnas;z++){
            suma_de_columnas+=matriz[i][z];
            suma_de_filas+=matriz[z][i];
       }
    for(int i=0;i<filas;i++){//ciclos que hacen lo mismo que el ciclo anterior, pero verificando las diagonales de la matriz
        suma_de_diagonales+=matriz[i][i];
    }
    for(int i=filas;i>0;i--){
        suma_de_diagonales2+=matriz[i-1][i-1];
    }
        if (suma_de_columnas==sumadecuadradomagico){//condicionales que miran que si en alguno de ellos no es igual a lo que deberia dar el cubo que se salga del ciclo de una vez
            if(suma_de_filas==sumadecuadradomagico){
                if (suma_de_diagonales==sumadecuadradomagico){
                    if (suma_de_diagonales2==sumadecuadradomagico){
                        continue;
                    }

                }
            }
        }
        else{//imprime de una vez si alguno de los condicionales de arriba no se cumplen
            cout<<"No es un cubo magico"<<endl;
            break;
        }
    }
if(suma_de_columnas==sumadecuadradomagico && suma_de_diagonales==sumadecuadradomagico && suma_de_filas==sumadecuadradomagico&& suma_de_diagonales2==sumadecuadradomagico){
    //si todas las sumas de filas, columnas y diagonales son iguales a lo que debe dar para que sea un cubo magico, que imprima que si lo es
    cout<<"La matriz es un cubo magico"<<endl;
}
    return 0;
}
